from django.db import models
import re

# Create your models here.
class Board(models.Model):
    boardUrl = models.CharField(max_length=5)
    boardTitle = models.CharField(max_length=100)
    hidden = models.BooleanField()
    lastPost = models.IntegerField(default=0)
    def __str__(self):
        return self.boardUrl

class Post(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    postContent = models.TextField(max_length=1000)
    mail = models.CharField(blank=True, max_length=20)
    password = models.CharField(max_length=64) #sha256 digest in hex
    ip = models.GenericIPAddressField(protocol='both')
    postDate = models.DateTimeField()
    postId = models.IntegerField()
    parentId = models.IntegerField(default=0)
    def __str__(self):
        return "{}/{}".format(self.board.boardUrl, self.postId)

class Wordfilter(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    pattern = models.CharField(max_length=255)
    repl = models.CharField(max_length=255)
    def apply(self, input_):
        return re.sub(self.pattern, self.repl, input_)

'''class Attachment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    fname = models.TextField(max_length=100)
    upload = models.FileField(upload_to=\
            '/'+self.post.board.boardUrl+'/res/%s')'''

