# Generated by Django 2.1 on 2018-08-26 12:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snekboard', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='parentId',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='post',
            name='mail',
            field=models.TextField(default='', max_length=20),
        ),
    ]
