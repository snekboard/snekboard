from django.apps import AppConfig


class SnekboardConfig(AppConfig):
    name = 'snekboard'
