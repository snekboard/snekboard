from django import template

register = template.Library()

def postprocess(text):
    text = text.replace('<<BANNED>>', '<br><b style="color: red;">(USER WAS BANNED FOR THIS POST)</b>')
    return text
register.filter('postprocess', postprocess)
