from django.contrib import admin

# Register your models here.

from .models import Board, Post, Wordfilter
class PostsInline(admin.TabularInline):
    model = Post
    extra = 1
    fields = ['postContent', 'mail', 'password', 'ip', 'postDate', 'postId', 'parentId'] 
class WordfilterInline(admin.TabularInline):
    model = Wordfilter
    extra = 2
    fields = ['pattern', 'repl']
class BoardAdmin(admin.ModelAdmin):
    fields = ['boardUrl', 'boardTitle', 'hidden']
    inlines = [WordfilterInline, PostsInline]
    list_display = ('boardUrl', 'boardTitle')
admin.site.register(Board, BoardAdmin)
