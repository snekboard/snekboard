from django.shortcuts import get_object_or_404 as get404, render
from django.http import *
from django.urls import reverse

from .models import Board, Post, Wordfilter

from hashlib import sha256
import datetime
from math import ceil
from django.contrib.auth.hashers import make_password, check_password

# Create your views here.

### HELPERS ###
def preprocess(text):
    text = text.replace('&', '&amp;')\
               .replace('<', '&lt;')\
               .replace('>', '&gt;')\
               .replace('\n', '<br>') #html escape

    text = text.replace('[b]', '<b>')\
               .replace('[/b]', '</b>')\
               .replace('[i]', '<i>')\
               .replace('[/i]', '</i>')\
               .replace('[u]', '<u>')\
               .replace('[/u]', '</u>')\
               .replace('[s]', '<s>')\
               .replace('[/s]', '</s>')\
               .replace('[code]', '<pre>')\
               .replace('[/code]', '</pre>') #bbcode

    return text

### VIEWS ###
def index(request):
    return render(request, 'snekboard/index.html')

POSTS_PER_PAGE = 5
def postsPage(request, b, p=1):
    board = get404(Board, boardUrl=b)
    posts = Post.objects.order_by('-postDate')
    parents = []
    for i in posts: # get threads sorted by last bump
        if i.parentId != 0:
            if i.mail != 'sage':
                parent = Post.objects.get(pk=i.parentId)
                if parent not in parents:
                    parents.append(parent)
        else:
            if i not in parents:
                parents.append(i)
    pages = ceil(len(parents) / POSTS_PER_PAGE)
    if pages < p: #there isn't a page 11 if there are 10 pages
        raise Http404
    last = []
    parents = parents[::-1] # reverse because paginator reverses
    for i in range( ((p-1)*POSTS_PER_PAGE)+1, p*POSTS_PER_PAGE+1):
        #paginate
        try:
            last.append(parents[0-i])
        except IndexError: #if not full page
            pass
    threads = []
    for i in last:
        ans = Post.objects.filter(parentId=i.postId).order_by('-postId')
        show = ans[:3][::-1]
        skipped = len(ans)-3 if len(ans) > 3 else 0
        threads.append([i, skipped, show])
    return render(request, 'snekboard/postsPage.html',
            {'t': threads, 'b': board, 'p': range(1, pages+1)})

def catalog(request, b):
    board = get404(Board, boardUrl=b)
    posts = Post.objects.order_by('-postDate')
    parents = []
    for i in posts: # get threads sorted by last bump
        if i.parentId != 0:
            if i.mail != 'sage':
                parent = Post.objects.get(pk=i.parentId)
                if parent not in parents:
                    parents.append(parent)
        else:
            if i not in parents:
                parents.append(i)
    posts = []
    for i in parents:
        posts.append({'mail': i.mail, 'postDate': i.postDate,
            'answ': Post.objects.filter(parentId=i.postId).count(),
            'postContent': i.postContent})
    return render(request, 'snekboard/catalog.html',
            {'b': board, 'posts': posts})

def post(request, b, p):
    post = get404(Post, board__boardUrl=b, postId=p)
    board = get404(Board, boardUrl=b)
    if post.parentId == 0:
        try:
            pid = post.postId
            ans = Post.objects.filter(parentId=pid)
        except DoesNotExist:
            ans = []
        return render(request, 'snekboard/post.html',
                {'b': board, 'post': post, 'ans': ans })
    else:
        return HttpResponseRedirect(reverse('snekboard:post', args=(b, post.parentId)))

def postapi(request, b):
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])
    board = get404(Board, boardUrl=b)
    pid = board.lastPost + 1
    P = request.POST
    if None in [P['msg'], P['mail'], P['parent'], P['pass']]:
        return HttpResponseBadRequest()
    if len(P['msg']) > 1000 or len(P['mail']) > 20:
        return HttpResponseBadRequest()
    msg = preprocess(P['msg'])
    for i in Wordfilter.objects.filter(board=board):
        msg = i.apply(msg)
    if int(P['parent']) != 0:
        if Post.objects.get(pk=P['parent']).parentId != 0:
            return HttpResponseBadRequest()
    p = Post(board=board, postContent=msg, mail=P['mail'],
             password = make_password(P['pass']),
             ip = request.META.get('X_FORWARDED_FOR') or \
                     request.META['REMOTE_ADDR'],
             postDate = datetime.datetime.now(), postId=pid,
             parentId = P['parent'])
    p.save()
    board.lastPost = pid
    board.save()
    return HttpResponseRedirect(reverse('snekboard:post', args=(b, p.postId)))

def postdelete(request, b):
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])
    board = get404(Board, boardUrl=b)
    P = request.POST
    if None in [P['post'], P['pass']]:
        return HttpResponseBadRequest()
    post = get404(Post, postId=P['post'], board=board)
    if check_password(P['pass'], post.password):
        parent = post.parentId
        if parent != 0:
            post.delete()
            return HttpResponseRedirect(reverse('snekboard:post', args=(b, parent)))
        else:
            for i in Post.objects.filter(parentId=post.postId):
                i.delete()
            post.delete()
            return HttpResponseRedirect(reverse('snekboard:postsPage', args=(b, 1)))
    else:
        return HttpResponseForbidden('Wrong password')
