from django.urls import path

from . import views

app_name = 'snekboard'
urlpatterns = [
    path('', views.index, name='index'),
    path('<b>/', views.postsPage, name='postsPage'),
    path('<b>/<int:p>/', views.postsPage, name='postsPage'),
    path('<b>/catalog/', views.catalog, name='catalog'),
    path('<b>/res/<int:p>', views.post, name='post'),
    path('<b>/post', views.postapi, name='postapi'),
    path('<b>/delete', views.postdelete, name='postdelete'),
]
